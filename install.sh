#!/bin/bash

#
#	Personalized install script to push ML build to a freshly formatted
#	card.
#

if [ ! -d /media/${USER}/EOS_DIGITAL ]; then
	echo "No EOS_DIGITAL card mounted for ${USER}."
	exit 1
fi

echo -n "Syncing Magic Lantern to card for 7D ... "
rsync -rtupP \
	--exclude exfat_sum \
       	--exclude package.sh \
       	--exclude build \
       	--exclude install.sh \
       	--exclude make_bootable.sh \
	--exclude README.md \
	--exclude '*.zip' \
	* /media/${USER}/EOS_DIGITAL/ 2>&1 > /dev/null
if [ $? -ne 0 ]; then
	echo "[ERROR]"
	exit 1
fi
echo "[done]"

echo "Making SD card bootable for ML ... "
sudo ./make_bootable.sh

echo -n "Syncing data to card ... "
sync; sync
echo "[done]"

