#!/bin/bash

echo -n "Creating build staging ... "
rm -rf build
mkdir -p build
rsync -rpP \
	--exclude exfat_sum \
       	--exclude package.sh \
       	--exclude build \
       	--exclude install.sh \
       	--exclude make_bootable.sh \
       	--exclude README.md \
       	--exclude '*.zip' \
	* build/ 2>&1 > /dev/null
if [ $? -ne 0 ]; then
	echo "[ERROR]"
	exit 1
fi
echo "[done]"

PKG="ML-7D-$(date +%Y-%m-%d).zip"
echo -n "Creating package $PKG ... "
( cd build ; zip -r "../$PKG" * 2>&1 > /dev/null )
rm -rf build 
echo "[done]"

