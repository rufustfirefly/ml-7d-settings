# MAGIC LANTERN FOR EOS 7D

## Prerequisites

 * Linux install (tested with [Ubuntu](http://www.ubuntu.com/))
 * Packages: ```dosfstools git rsync``` (these are the relevant Debian/Ubuntu
   packages, this may be different for other distributions)

## Installation/Updating

1. **Prepare a card**. You can either freshly format a card in the camera, or
   just make sure that your card has the label ```EOS_DIGITAL```. If you need
   to relabel an existing card, you can use 
   ```dosfslabel /dev/(DEVICE) EOS_DIGITAL```.
2. **Pull down a copy of this distribution**. You can do this with the command
   ```git clone https://bitbucket.org/rufustfirefly/ml-7d-settings.git```.
   From this directory, you can update by using ```git pull```.
3. **Run install.sh**. From this directory, run ```./install.sh```. This will
   both prepare your card as a "bootable" ML card, as well as copy the
   Magic Lantern distribution onto the card.

